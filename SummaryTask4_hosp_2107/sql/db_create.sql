SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET NAMES utf8;
drop database if exists hospdb;
create database hospdb;
use hospdb;

CREATE TABLE IF NOT EXISTS hosp_cards (
    id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    p_diagnosis varchar(170)
) DEFAULT CHARSET=utf8;

insert into hosp_cards (id, p_diagnosis) values
(1, 'Грыжа'),
(2, 'Отёк'),
(3, 'Гайморрит'),
(4, 'Отравление грибами'),
(5, 'Перелом бедра'),
(6, 'Рана на руке'),
(7, 'Ушиб кисти'),
(8, 'Диарея'),
(9, 'Запор');

CREATE TABLE users_roles (
  id int(11) NOT NULL PRIMARY KEY,
  name varchar(20) NOT NULL UNIQUE
) DEFAULT CHARSET=utf8;

INSERT INTO users_roles (id, name) VALUES
(0, 'admin'),
(1, 'doctor'),
(2, 'nurse');


CREATE TABLE IF NOT EXISTS specializations (
  id int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title varchar(40) NOT NULL
) DEFAULT CHARSET=utf8;

INSERT INTO specializations (id, title) VALUES
(1, 'Pediatrician'),
(2, 'Traumatologist'),
(3, 'Surgeon');



CREATE TABLE IF NOT EXISTS types_of_treatment (
  id int(20) NOT NULL AUTO_INCREMENT,
  title varchar(40) NOT NULL,
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;


INSERT INTO types_of_treatment (id, title) VALUES
(1, 'Procedure'),
(2, 'Medicine'),
(3, 'Operation');

CREATE TABLE treatments (
    id INT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    hosp_card_id INT(20) NOT NULL,
    type_of_treatment_id INT(20) NOT NULL,
    name_of_medication VARCHAR(50) NOT NULL,
    done TINYINT(1) DEFAULT NULL,
	foreign key (hosp_card_id) references hosp_cards(id),
    foreign key (type_of_treatment_id) references types_of_treatment(id)
) DEFAULT CHARSET=utf8;

INSERT INTO treatments (id, hosp_card_id, type_of_treatment_id, name_of_medication, done) VALUES
(1, 1, 2, 'Детралекс', 1),
(2, 2, 2, 'Назолин', 0),
(3, 3, 2, 'Метилурацил', 0),
(4, 4, 3, 'Персен', 0),
(5, 5, 2, 'Диазол', 0),
(6, 5, 2, 'Перекись водорода', 0),
(7, 5, 2, 'Левомитецин', 0),
(8, 6, 2, 'Омепразол', 0),
(9, 6, 2, 'Уголь активированный', 0),
(10, 6, 2, 'Линекс', 0),
(11, 7, 2, 'Стрепсилс', 0),
(12, 7, 1, 'Аспирин', 0),
(13, 7, 2, 'Рентген', 0),
(14, 8, 2, 'Зеленка', 0),
(15, 8, 1, 'Перевязка', 0),
(16, 8, 1, 'УЗИ', 0),
(17, 9, 2, 'Томография', 0),
(18, 9, 2, 'Долобене', 0),
(19, 9, 1, 'Занятие в тренажерном зале', 0);

CREATE TABLE IF NOT EXISTS users (
  id int(10) NOT NULL AUTO_INCREMENT,
  login varchar(20) DEFAULT NULL UNIQUE,
  password varchar(20) DEFAULT NULL,
  first_name varchar(30) DEFAULT NULL,
  last_name varchar(30) DEFAULT NULL,
  role_id int(10) DEFAULT NULL,
  specialization_id int(10) DEFAULT NULL,
  count_of_patients int(10) DEFAULT NULL,
  PRIMARY KEY (id),
  foreign key (role_id) references users_roles(id),
  foreign key (specialization_id) references specializations(id)
) DEFAULT CHARSET=utf8;

INSERT INTO `users` (id, login, password, first_name, last_name, role_id, specialization_id, count_of_patients) VALUES
(1, 'admin', 'admin', 'Sergey', 'Sergeev',  0, NULL, 0),
(2, 'doctor', 'doctor', 'Ivan', 'Ivanov',  1, 1, 4),
(3, 'doc1', 'doc1', 'Bruce', 'Lee',  1, 3, 2),
(6, 'nurse', 'nurse', 'Татьяна', 'Потемкина',  2, NULL, NULL),
(7, 'nurse1', 'nurse2', 'Светлана2', 'Казакова2',  2, NULL, NULL),
(8, 'karl', 'karl', 'Карл', 'Шафраненко', 1, 2, 3);

CREATE TABLE patients (
    id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(40) NOT NULL,
    last_name VARCHAR(40) NOT NULL,
    birthday DATE DEFAULT NULL,
    doctor_id INT(10) DEFAULT NULL,
    card_id INT(10) DEFAULT NULL,
    foreign key (doctor_id) references users(id),
    foreign key (card_id) references hosp_cards(id)
) DEFAULT CHARSET=utf8;

INSERT INTO patients (id, first_name, last_name, birthday, doctor_id, card_id) VALUES
(1, 'Андрей', 'Майоров', '2013-07-08', 2, 1),
(2, 'Марьяна', 'Пешкова', '2011-08-13', 2, 2),
(3, 'Дмитро', 'Матвейченко', '2008-07-30', 2, 3),
(4, 'Василий', 'Фидляр', '2007-08-04', 2, 4),
(5, 'Александр', 'Петренко', '2000-08-17', 8, 5),
(6, 'Владимир', 'Машков', '1980-08-24', 8, 6),
(7, 'Гуля', 'Абрамов', '2001-08-04', 8, 7),
(8, 'Иван', 'Нагорный', '1979-08-05', 3, 8),
(9, 'Мария', 'Филатова', '1999-08-20', 3, 9);


CREATE TABLE IF NOT EXISTS discharged_patients (
    id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(40) DEFAULT NULL,
    last_name VARCHAR(40) DEFAULT NULL,
    birthday DATE DEFAULT NULL,
    doctor_id INT(10) DEFAULT NULL,
    foreign key (doctor_id) references users(id)
) DEFAULT CHARSET=utf8;

INSERT INTO discharged_patients (id, first_name, last_name, birthday, doctor_id) VALUES
(35, 'Дмитрий', 'Дмитриев', '1976-07-30', 2);

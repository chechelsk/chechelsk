package ua.nure.chechel.SummaryTask4;

/**
 * Contains all paths.
 * 
 * @author Sergey Chechel
 *
 */
public class AllPathes {

	public static final String PATH_ERROR_PAGE = "/WEB-INF/jsp/pageError.jsp";
	public static final String PATH_WELCOME_PAGE = "loginPage.jsp";

	public static final String PATH_FORWARD_VIEW_ALL_DOCTORS = "/WEB-INF/jsp/listDoctors.jsp";
	public static final String PATH_FORWARD_VIEW_ALL_PATIENTS = "/WEB-INF/jsp/listPatients.jsp";
	public static final String PATH_FORWARD_VIEW_DISCHARGED_PATIENTS = "/WEB-INF/jsp/listDischargedPatients.jsp";
	public static final String PATH_FORWARD_USER_ADD = "/WEB-INF/jsp/addUser.jsp";
	public static final String PATH_FORWARD_PATIENT_ADD = "/WEB-INF/jsp/addPatient.jsp";
	public static final String PATH_FORWARD_HOSPITAL_CARD = "/WEB-INF/jsp/hospitalCard.jsp";
	
	//task
	public static final String PATH_FORWARD_GET_PATIENTS = "/WEB-INF/jsp/getViewPatients.jsp";

	public static final String PATH_RDRT_TO_VIEW_ALL_DOCTORS = "controller?comand=listDoctors";
	public static final String PATH_RDRT_TO_VIEW_ALL_PATIENTS = "controller?comand=listPatients";
	public static final String PATH_RDRT_TO_VIEW_DISCHARGED_PATIENTS = "controller?comand=listDischargedPatients";
	public static final String PATH_RDRT_TO_VIEW_HOSPITAL_CARD = "controller?comand=hospitalCard";
	public static final String PATH_RDRT_TO_VIEW_PATIENTS_BY_DOCTOR_ID = "controller?comand=listPatientsByDoctorId";
	public static final String PATH_RDRT_TO_VIEW_ADD_USER_FORM = "controller?comand=addUser";
	public static final String PATH_RDRT_TO_VIEW_ADD_PATIENT_FORM = "controller?comand=addPatient";
	public static final String PATH_RDRT_TO_LOGIN_FORM = "controller?comand=login";

}

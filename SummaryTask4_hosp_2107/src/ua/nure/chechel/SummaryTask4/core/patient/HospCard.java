package ua.nure.chechel.SummaryTask4.core.patient;

import ua.nure.chechel.SummaryTask4.core.Entity;

/**
 * Class for Hospital card entity.
 * 
 * @author Sergey Chechel
 *
 */
public class HospCard extends Entity {

	private static final long serialVersionUID = 8924384621418707436L;

	private String p_diagnosis;

	public HospCard(int id, String diagnos) {
		this.setId(id);
		this.p_diagnosis = diagnos;
	}

	public String getDiagnosis() {
		return p_diagnosis;
	}

	public void setDiagnosis(String diagnos) {
		this.p_diagnosis = diagnos;
	}

}

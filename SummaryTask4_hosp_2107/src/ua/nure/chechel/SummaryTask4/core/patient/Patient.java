package ua.nure.chechel.SummaryTask4.core.patient;

import java.sql.Date;

import ua.nure.chechel.SummaryTask4.core.Entity;

/**
 * Class for Patient entity.
 * 
 * @author Sergey Chechel
 *
 */
public class Patient extends Entity {

	private static final long serialVersionUID = 2613496056932204312L;

	private String firstName;
	private String lastName;
	private Date birthday;
	private int doctorId;
	private int cardId;
	private int countOfTreatments;
	
	public Patient(int id, String first_Name, String last_Name, Date birth_day, int doctor_Id, int card_Id) {
		this.setId(id);
		this.firstName = first_Name;
		this.lastName = last_Name;
		this.birthday = birth_day;
		this.doctorId = doctor_Id;
		this.cardId = card_Id;

	}
	
	public Patient(int id, String first_Name, String last_Name, Date birth_day, int doctor_Id) {
		this.setId(id);
		this.firstName = first_Name;
		this.lastName = last_Name;
		this.birthday = birth_day;
		this.doctorId = doctor_Id;
	}
	
	public Patient(String first_Name, String last_Name, Date birth_day) {
		this.firstName = first_Name;
		this.lastName = last_Name;
		this.birthday = birth_day;
	}
	
	public Patient(int id, int count) {
		this.setId(id);
		this.countOfTreatments = count;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String first_Name) {
		this.firstName = first_Name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String last_Name) {
		this.lastName = last_Name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}
	
	public int getCountOfTreat() {
		return countOfTreatments;
	}

	public void setCountOfTreat(int count) {
		this.countOfTreatments = count;
	}

}

package ua.nure.chechel.SummaryTask4.core.patient;

import java.util.List;

/**
 * Patient dao interface.
 * 
 * @author Sergey Chechel
 *
 */
public interface PatientDao {

	/**
	 * Get all patients.
	 * 
	 * @return list of patients.
	 */
	List<Patient> getAllPatients();

	/**
	 * Get treatments by hospital card id.
	 * 
	 * @param cardId
	 *            specified hospital card id.
	 * @return list of treatments.
	 */
	List<Treatment> getAllTreatmentsByCardId(int cardId);

	/**
	 * Get patients by doctor id.
	 * 
	 * @param doctorId
	 *            specified doctor id.
	 * @return list of patients
	 */
	List<Patient> getAllPatientsByDoctorId(int doctorId);

	/**
	 * Get all types of treatment.
	 * 
	 * @return list of types of treatment.
	 */
	List<TypeOfTreatment> getAllTypesOfTreatment();

	/**
	 * Get discharged patients by doctor id.
	 * 
	 * @param doctorId
	 *            specified doctor id.
	 * @return list of patients.
	 */
	List<Patient> getDischargedPatientsByDoctorId(int doctorId);

	/**
	 * Get hospital card by id.
	 * 
	 * @param id
	 *            specified id.
	 * @return hospital card entity.
	 */
	HospCard getHospCardById(int id);

	/**
	 * Get patient by hospital card id.
	 * 
	 * @param hospitalCardId
	 *            specified hospital card id.
	 * @return patient entity.
	 */
	Patient getPatientByHospCardId(int hospitalCardId);

	/**
	 * Create hospital card for new patient.
	 * 
	 * @return hospital card id.
	 */
	int addHospCard();

	/**
	 * Complete course of treatment for specified patient.
	 * 
	 * @param patient
	 *            specified patient.
	 */
	void completeCourseOfTreatment(Patient patient);

	/**
	 * Add patient.
	 * 
	 * @param patient
	 *            specified patient.
	 */
	void addPatient(Patient patient);

	/**
	 * Set doctor to patient.
	 * 
	 * @param parientId
	 *            specified patient id.
	 * @param doctorId
	 *            specified doctor id.
	 */
	void setDoctorToThePatient(int parientId, int doctoeId);

	/**
	 * Update patient diagnosis.
	 * 
	 * @param cardId
	 *            specified hospital card id.
	 * @param diagnosis
	 *            new diagnosis.
	 */
	void updateDiagnosisInHospCard(int cardId, String diagnosis);

	/**
	 * Add new treatment.
	 * 
	 * @param treatment
	 *            specified treatment.
	 */
	void addTreatment(Treatment treatment);

	/**
	 * Finish treatment.
	 * 
	 * @param treatmentId
	 *            specified treatment id.
	 */
	void finishTreatment(int treatmentId);

	/**
	 * Get patients by type of treatment 'operation'
	 * 
	 * @return list patients.
	 */
	List<Patient> getAllPatientsByTreatmentOperation();

	List<Patient> getAllPatientsWithCountOfTreatments();

}

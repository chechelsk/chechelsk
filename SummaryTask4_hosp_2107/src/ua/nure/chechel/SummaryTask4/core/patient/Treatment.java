package ua.nure.chechel.SummaryTask4.core.patient;

import ua.nure.chechel.SummaryTask4.core.Entity;

/**
 * Treatment entity.
 * 
 * @author Sergey Chechel
 *
 */
public class Treatment extends Entity {

	private static final long serialVersionUID = -897453822651366267L;
	
	private int typeOfTreatmentId;
	private int hospitalCardId;
	private String nameOfMedication;
	private boolean done;
	
	public Treatment(int id, int typeOf_TreatmentId, int hospital_CardId, String nameOf_Medication, boolean done) {
		this.setId(id);
		this.typeOfTreatmentId = typeOf_TreatmentId;
		this.hospitalCardId = hospital_CardId;
		this.nameOfMedication = nameOf_Medication;
		this.done = done;
	}
	
	public Treatment(int typeOf_TreatmentId, int hospital_CardId, String nameOf_Medication) {
		this.typeOfTreatmentId = typeOf_TreatmentId;
		this.hospitalCardId = hospital_CardId;
		this.nameOfMedication = nameOf_Medication;
	}

	public int getTypeOfTreatmentId() {
		return typeOfTreatmentId;
	}

	public void setTypeOfTreatmentId(int typeOf_TreatmentId) {
		this.typeOfTreatmentId = typeOf_TreatmentId;
	}

	public int getHospitalCardId() {
		return hospitalCardId;
	}

	public void setHospitalCardId(int hospital_CardId) {
		this.hospitalCardId = hospital_CardId;
	}

	public String getNameOfMedication() {
		return nameOfMedication;
	}

	public void setNameOfMedication(String nameOf_Medication) {
		this.nameOfMedication = nameOf_Medication;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

}

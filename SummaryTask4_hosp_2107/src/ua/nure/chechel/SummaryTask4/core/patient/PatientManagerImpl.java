package ua.nure.chechel.SummaryTask4.core.patient;

import java.util.List;
import ua.nure.chechel.SummaryTask4.dao.impl.PatientDaoImpl;

/**
 * Patient manager implementation. Here the business logic call dao methods.
 * 
 * @author Sergey Chechel
 *
 */
public class PatientManagerImpl implements PatientManager {
	
	private PatientDao patientDao = new PatientDaoImpl();

	@Override
	public List<Patient> getAllPatients() {
		return patientDao.getAllPatients();
	}

	@Override
	public void addPatient(Patient patient) {
		patientDao.addPatient(patient);

	}

	@Override
	public void setDoctorToThePatient(int parientId, int doctorId) {
		patientDao.setDoctorToThePatient(parientId, doctorId);

	}

	@Override
	public void updateDiagnosisInHospCard(int cardId, String diagnosis) {
		patientDao.updateDiagnosisInHospCard(cardId, diagnosis);

	}

	@Override
	public HospCard getHospCardById(int id) {
		return patientDao.getHospCardById(id);
	}

	@Override
	public int addHospCard() {
		return patientDao.addHospCard();
	}

	@Override
	public List<Treatment> getAllTreatmentsByCardId(int cardId) {
		return patientDao.getAllTreatmentsByCardId(cardId);
	}

	@Override
	public void addTreatment(Treatment treatment) {
		patientDao.addTreatment(treatment);
	}

	@Override
	public void finishTreatment(int treatmentId) {
		patientDao.finishTreatment(treatmentId);
	}

	@Override
	public List<Patient> getAllPatientsByDoctorId(int doctorId) {
		return patientDao.getAllPatientsByDoctorId(doctorId);
	}

	@Override
	public List<TypeOfTreatment> getAllTypesOfTreatment() {
		return patientDao.getAllTypesOfTreatment();
	}

	@Override
	public List<Patient> getDischargedPatientsByDoctorId(int doctorId) {
		return patientDao.getDischargedPatientsByDoctorId(doctorId);
	}

	@Override
	public void completeCourseOfTreatment(Patient patient) {
		patientDao.completeCourseOfTreatment(patient);
	}

	@Override
	public Patient getPatientByHospCardId(int hospitalCardId) {
		return patientDao.getPatientByHospCardId(hospitalCardId);
	}

	@Override
	public List<Patient> getAllPatientsByTreatmentOperation() {
		return patientDao.getAllPatientsByTreatmentOperation();
	}
	
	@Override
	public List<Patient> getAllPatientsWithCountOfTreatments() {
		return patientDao.getAllPatientsWithCountOfTreatments();
	}

}

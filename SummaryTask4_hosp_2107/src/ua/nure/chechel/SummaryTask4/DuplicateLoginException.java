package ua.nure.chechel.SummaryTask4;

/**
 * Throws when in database was found same login.
 * 
 * @author Sergey Chechel
 *
 */
public class DuplicateLoginException extends Exception {

	private static final long serialVersionUID = -6003145638521255299L;

	public DuplicateLoginException() {

	}

	public DuplicateLoginException(String message) {
		super(message);
	}

	public DuplicateLoginException(Throwable cause) {
		super(cause);
	}

	public DuplicateLoginException(String message, Throwable cause) {
		super(message, cause);
	}
}

package ua.nure.chechel.SummaryTask4.dao;

/**
 * Contains all querys to database.
 * 
 * @author Sergey Chechel
 *
 */
public class Query {

	// user
	public static final String SQL_SELECT_ALL_DOCTORS = "SELECT * FROM users WHERE role_id = 1";
	public static final String SQL_SELECT_DOCTORS_BY_SPEC = "SELECT * FROM users WHERE specialization_id = (?)";
	public static final String SQL_INSERT_USER = "INSERT INTO users (login, password, first_name, last_name, role_id, specialization_id, count_of_patients) VALUES (?, ?, ?, ?,  ?, ?, ?)";
	public static final String SQL_SELECT_USER_BY_LOGIN = "SELECT * FROM users WHERE login = (?)";
	public static final String SQL_SELECT_USER_BY_ID = "SELECT * FROM users WHERE id = (?)";
	public static final String SQL_SELECT_ALL_ROLES = "SELECT * FROM users_roles";
	public static final String SQL_SELECT_ALL_SPECIALIZATIONS = "SELECT * FROM specializations";
	public static final String SQL_INCREMENT_COUNT_OF_PATIENTS = "UPDATE users SET count_of_patients=count_of_patients+1 WHERE id=?";
	public static final String SQL_DECREMENT_COUNT_OF_PATIENTS = "UPDATE users SET count_of_patients=count_of_patients-1 WHERE id=?";

	// patient
	public static final String SQL_SELECT_ALL_PATIENTS = "SELECT * FROM patients;";
	public static final String SQL_SELECT_PATIENTS_BY_DOCTOR_ID = "SELECT * FROM patients WHERE doctor_id = ?";
	public static final String SQL_INSERT_PATIENT = "INSERT INTO patients (first_name, last_name, birthday, card_id) VALUES (?,?,?,?)";
	public static final String SQL_SET_DOCTOR_TO_PATIENT = "UPDATE patients SET doctor_id = ? WHERE id = ?";
	public static final String SQL_UPDATE_DIAGNOSIS = "UPDATE hosp_cards SET p_diagnosis = ? WHERE id = ?";
	public static final String SQL_CREATE_HOSPITAL_CARD = "INSERT INTO hosp_cards (p_diagnosis) VALUE (null)";
	public static final String SQL_INSERT_TREATMENT = "INSERT INTO treatments (hosp_card_id, type_of_treatment_id, name_of_medication) VALUES (?, ?, ?)";
	public static final String SQL_UPDATE_FINISH_TREATMENT = "UPDATE treatments SET done = 1 WHERE id = ?";
	public static final String SQL_SELECT_HOSPITAL_CARD_BY_ID = "SELECT * FROM hosp_cards WHERE id = ?";
	public static final String SQL_SELECT_TREATMENTS_BY_HOSPITAL_CARD = "SELECT * FROM treatments WHERE hosp_card_id = (?)";
	public static final String SQL_SELECT_ALL_TYPES_OF_TREATMENT = "SELECT * FROM types_of_treatment";
	public static final String SQL_SELECT_PATIENT_BY_HOSPITAL_CARD_ID = "SELECT * FROM patients WHERE card_id = ?";
	public static final String SQL_SELECT_DISCHARGED_PATIENTS_BY_DOCTOR_ID = "SELECT * FROM discharged_patients WHERE doctor_id = ?";
	public static final String SQL_DELETE_PATIENT_BY_ID = "DELETE FROM patients WHERE id = ?";
	public static final String SQL_DELETE_HOSPITAL_CARD_BY_ID = "DELETE FROM hosp_cards WHERE id = ?";
	public static final String SQL_INSERT_DISCHARGED_PATIENT = "INSERT INTO discharged_patients (first_name, last_name, birthday, doctor_id) VALUES (?, ?, ?, ?)";
	public static final String SQL_DELETE_TREATMENTS_BY_HOSPITAL_CARD_ID = "DELETE FROM treatments WHERE hosp_card_id = ?";
	
	//task
	public static final String SQL_SELECT_PATIENTS_BY_DIAGNOSIS = "select * from patients inner join treatments on patients.card_id = treatments.hosp_card_id group by patients.card_id"; /* where type_of_treatment_id = 2   */
	public static final String SQL_SELECT_TREATMENTS_BY_PATIENT = "SELECT patients.id, COUNT(*) as count FROM treatments inner join patients WHERE treatments.hosp_card_id = patients.card_id and treatments.done = 1 group by patients.card_id";
}

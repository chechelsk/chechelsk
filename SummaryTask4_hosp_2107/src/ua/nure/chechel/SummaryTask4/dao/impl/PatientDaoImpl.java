package ua.nure.chechel.SummaryTask4.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.core.patient.HospCard;
import ua.nure.chechel.SummaryTask4.core.patient.Patient;
import ua.nure.chechel.SummaryTask4.core.patient.PatientDao;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManager;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManagerImpl;
import ua.nure.chechel.SummaryTask4.core.patient.Treatment;
import ua.nure.chechel.SummaryTask4.core.patient.TypeOfTreatment;
import ua.nure.chechel.SummaryTask4.dao.ConnectionPool;
import ua.nure.chechel.SummaryTask4.dao.Query;

/**
 * An implementation of PatientDao interface.
 * 
 * @author Sergey Chechel
 *
 */
public class PatientDaoImpl implements PatientDao {

	private final static Logger LOG = Logger.getLogger(PatientDaoImpl.class);

	private Connection connection;

	@Override
	public List<Patient> getAllPatients() {
		List<Patient> patients = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(Query.SQL_SELECT_ALL_PATIENTS)) {
			while (rs.next()) {
				patients.add(new Patient(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
						rs.getDate("birthday"), rs.getInt("doctor_id"), rs.getInt("card_id")));
			}

		} catch (SQLException ex) {
			LOG.error("Can not find patients", ex);
		} finally {
			closeConnection();
		}
		return patients;
	}

	@Override
	public List<TypeOfTreatment> getAllTypesOfTreatment() {
		List<TypeOfTreatment> typesOfTreatment = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(Query.SQL_SELECT_ALL_TYPES_OF_TREATMENT)) {
			while (rs.next()) {
				
				// create entity set id and add to list
				for(TypeOfTreatment typeOfTreatment : TypeOfTreatment.values()) {
					if (rs.getString("title").toUpperCase().equals(typeOfTreatment.toString())) {
						typeOfTreatment.setId(rs.getInt("id"));
						typesOfTreatment.add(typeOfTreatment);
						break;
					}
				}
			}

		} catch (SQLException ex) {
			LOG.error("Can not find types of treatment", ex);
		} finally {
			closeConnection();
		}
		return typesOfTreatment;
	}

	@Override
	public List<Patient> getAllPatientsByDoctorId(int doctorId) {
		List<Patient> patients = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SQL_SELECT_PATIENTS_BY_DOCTOR_ID)) {
			pStatement.setInt(1, doctorId);
			pStatement.execute();

			ResultSet rs = pStatement.getResultSet();
			while (rs.next()) {
				patients.add(new Patient(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
						rs.getDate("birthday"), rs.getInt("doctor_id"), rs.getInt("card_id")));
			}

		} catch (SQLException ex) {
			LOG.error("Can not find patients by doctor id", ex);
		} finally {
			closeConnection();
		}
		return patients;
	}

	@Override
	public void addPatient(Patient patient) {
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SQL_INSERT_PATIENT)) {
			pStatement.setString(1, patient.getFirstName());
			pStatement.setString(2, patient.getLastName());
			pStatement.setDate(3, patient.getBirthday());
			pStatement.setInt(4, patient.getCardId());
			pStatement.executeUpdate();

		} catch (SQLException ex) {
			LOG.error("Can not create a new patient", ex);
		} finally {
			closeConnection();
		}

	}

	@Override
	public void setDoctorToThePatient(int parientId, int doctorId) {
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SQL_SET_DOCTOR_TO_PATIENT);
				PreparedStatement psIncrement = connection.prepareStatement(Query.SQL_INCREMENT_COUNT_OF_PATIENTS)) {
			
			connection.setAutoCommit(false);

			pStatement.setInt(1, doctorId);
			pStatement.setInt(2, parientId);
			pStatement.executeUpdate();

			psIncrement.setInt(1, doctorId);
			psIncrement.executeUpdate();

			connection.commit();
			
		} catch (SQLException ex) {
			LOG.error("Can`t set the doctor to the patient", ex);
		} finally {
			closeConnection();
		}

	}

	@Override
	public void updateDiagnosisInHospCard(int cardId, String diagnosis) {
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SQL_UPDATE_DIAGNOSIS)) {
			pStatement.setString(1, diagnosis);
			pStatement.setInt(2, cardId);
			pStatement.executeUpdate();
		} catch (SQLException ex) {
			LOG.error("Can not update diagnosis", ex);
		} finally {
			closeConnection();
		}

	}

	@Override
	public int addHospCard() {
		int cardId = 0;
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(Query.SQL_CREATE_HOSPITAL_CARD, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = statement.getGeneratedKeys();
			if (rs != null && rs.next()) {
				cardId = rs.getInt(1);
			}
		} catch (SQLException ex) {
			LOG.error("Can not create hospital card", ex);
		} finally {
			closeConnection();
		}

		return cardId;
	}

	@Override
	public List<Treatment> getAllTreatmentsByCardId(int cardId) {
		List<Treatment> treatments = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (final PreparedStatement prStatement = this.connection
				.prepareStatement(Query.SQL_SELECT_TREATMENTS_BY_HOSPITAL_CARD)) {
			prStatement.setInt(1, cardId);
			ResultSet rs = prStatement.executeQuery();
			while (rs.next()) {
				treatments.add(new Treatment(rs.getInt("id"), rs.getInt("type_of_treatment_id"),
						rs.getInt("hosp_card_id"), rs.getString("name_of_medication"), rs.getBoolean("done")));
			}
			rs.close();
		} catch (SQLException e) {
			LOG.error("Can not find treatments by hospitalcard id");
		} finally {
			closeConnection();
		}

		return treatments;
	}

	@Override
	public void addTreatment(Treatment treatment) {
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SQL_INSERT_TREATMENT)) {
			pStatement.setInt(1, treatment.getHospitalCardId());
			pStatement.setInt(2, treatment.getTypeOfTreatmentId());
			pStatement.setString(3, treatment.getNameOfMedication());
			pStatement.executeUpdate();

		} catch (SQLException ex) {
			LOG.error("Can not create a new treatment", ex);
		} finally {
			closeConnection();
		}

	}

	@Override
	public void finishTreatment(int treatmentId) {
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection.prepareStatement(Query.SQL_UPDATE_FINISH_TREATMENT)) {
			pStatement.setInt(1, treatmentId);
			pStatement.executeUpdate();

		} catch (SQLException ex) {
			LOG.error("Can not finish treatment", ex);
		} finally {
			closeConnection();
		}

	}

	@Override
	public HospCard getHospCardById(int id) {
		connection = ConnectionPool.getConnection();
		HospCard card = null;
		try (PreparedStatement ps = connection.prepareStatement(Query.SQL_SELECT_HOSPITAL_CARD_BY_ID)) {
			ps.setInt(1, id);
			ps.execute();

			ResultSet rs = ps.getResultSet();
			if (rs.next()) {
				card = new HospCard(rs.getInt("id"), rs.getString("p_diagnosis"));
			}
			rs.close();
		} catch (SQLException ex) {
			LOG.error("Can not find hospital card by id", ex);
		} finally {
			closeConnection();
		}

		return card;
	}

	@Override
	public Patient getPatientByHospCardId(int hospitalCardId) {
		connection = ConnectionPool.getConnection();
		Patient patient = null;
		try (PreparedStatement ps = connection.prepareStatement(Query.SQL_SELECT_PATIENT_BY_HOSPITAL_CARD_ID)) {
			ps.setInt(1, hospitalCardId);
			ps.execute();

			ResultSet rs = ps.getResultSet();
			if (rs.next()) {
				patient = new Patient(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
						rs.getDate("birthday"), rs.getInt("doctor_id"), rs.getInt("card_id"));
			}
			rs.close();
		} catch (SQLException ex) {
			LOG.error("Can not find patient by hospital card id", ex);
		} finally {
			closeConnection();
		}

		return patient;
	}

	@Override
	public List<Patient> getDischargedPatientsByDoctorId(int doctorId) {
		List<Patient> patients = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (PreparedStatement pStatement = connection
				.prepareStatement(Query.SQL_SELECT_DISCHARGED_PATIENTS_BY_DOCTOR_ID)) {
			pStatement.setInt(1, doctorId);
			pStatement.execute();

			ResultSet rs = pStatement.getResultSet();
			while (rs.next()) {
				patients.add(new Patient(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
						rs.getDate("birthday"), rs.getInt("doctor_id")));
			}

		} catch (SQLException ex) {
			LOG.error("Can not find discharged patients by doctor id", ex);
		} finally {
			closeConnection();
		}
		return patients;
	}

	@Override
	public void completeCourseOfTreatment(Patient patient) {
		connection = ConnectionPool.getConnection();
		try (PreparedStatement psPatient = connection.prepareStatement(Query.SQL_DELETE_PATIENT_BY_ID);
			 PreparedStatement psHospitalCard = connection.prepareStatement(Query.SQL_DELETE_HOSPITAL_CARD_BY_ID);
			 PreparedStatement psDoctorCount = connection.prepareStatement(Query.SQL_DECREMENT_COUNT_OF_PATIENTS);
			 PreparedStatement psDischargedPatient = connection.prepareStatement(Query.SQL_INSERT_DISCHARGED_PATIENT);
			 PreparedStatement psTreatments = connection.prepareStatement(Query.SQL_DELETE_TREATMENTS_BY_HOSPITAL_CARD_ID);) {
			
			connection.setAutoCommit(false);

			LOG.trace("Patient data: id: " + patient.getId() + ", firstName: " + patient.getFirstName() + ", lastName: "
					+ patient.getLastName() + ", dateOfBirth: " + patient.getBirthday() + ", cardId: " + patient.getCardId()
					+ ", doctorID: " + patient.getDoctorId());

			// set patient id for delete
			psPatient.setInt(1, patient.getId());
			psPatient.executeUpdate();

			// set hospital card id for delete treatments
			psTreatments.setInt(1, patient.getCardId());
			psTreatments.executeUpdate();
			
			// set hospital card id for delete
			psHospitalCard.setInt(1, patient.getCardId());
			psHospitalCard.executeUpdate();

			// set doctor id for decrement count of patients 
			psDoctorCount.setInt(1, patient.getDoctorId());
			psDoctorCount.executeUpdate();

			// set data for insert new discharged patient 
			psDischargedPatient.setString(1, patient.getFirstName());
			psDischargedPatient.setString(2, patient.getLastName());
			psDischargedPatient.setDate(3, patient.getBirthday());
			psDischargedPatient.setInt(4, patient.getDoctorId());
			psDischargedPatient.executeUpdate();

			connection.commit();
		} catch (SQLException ex) {
			LOG.error("Can not compleate the course of treatment", ex);
		} finally {
			closeConnection();
		}

	}

	/**
	 * The method closes connection.
	 */
	private void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				LOG.error("Can not close connection");
			}
		}
	}

	@Override
	public List<Patient> getAllPatientsByTreatmentOperation() {
		List<Patient> patients = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(Query.SQL_SELECT_PATIENTS_BY_DIAGNOSIS)) {
			while (rs.next()) {
				patients.add(new Patient(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
						rs.getDate("birthday"), rs.getInt("doctor_id"), rs.getInt("card_id")));
			}

		} catch (SQLException ex) {
			LOG.error("Can`t find such patients", ex);
		} finally {
			closeConnection();
		}
		return patients;
	}
	
	@Override
	public List<Patient> getAllPatientsWithCountOfTreatments() {
		PatientManager manager = new PatientManagerImpl();
		List<Patient> patients = manager.getAllPatients();
		
		List<Patient> treatmentsToPpatients = new ArrayList<>();
		connection = ConnectionPool.getConnection();
		try (Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(Query.SQL_SELECT_TREATMENTS_BY_PATIENT)) {
			while (rs.next()) {
				treatmentsToPpatients.add(new Patient(rs.getInt("id"), rs.getInt("count")));
			}
			
			for (Patient patient : patients) {
				for (Patient p : treatmentsToPpatients) {
					if (patient.getId() == p.getId()) {
						patient.setCountOfTreat(p.getCountOfTreat());
					}
				}
			}

		} catch (SQLException ex) {
			LOG.error("Can`t find such treatments", ex);
		} finally {
			closeConnection();
		}
		return patients;
	}
	
}



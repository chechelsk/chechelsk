package ua.nure.chechel.SummaryTask4.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.web.comands.Command;
import ua.nure.chechel.SummaryTask4.web.comands.ManagerCommand;

/**
 * Servlet implementation class Controller. This servlet handles all
 * requests by the client and then processes them according to specified comand
 * name.
 */
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(Controller.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response, TypeAction.GET);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response, TypeAction.POST);
	}

	/**
	 * Handles all requests coming from the client by executing the specified
	 * comand name in a request. Implements PRG pattern by checking action type
	 * specified by the invoked method.
	 *
	 * @param request
	 * @param response
	 * @param actionType
	 * @throws IOException
	 * @throws ServletException
	 * @see TypeAction
	 */
	private void process(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {

		LOG.debug("Start processing in Controller");

		// extract name of comand  from the request
		String nameOfCommand = request.getParameter("comand");
		LOG.trace("Request parameter: 'comand' = " + nameOfCommand);

		// obtain comand object by its name
		Command comand = ManagerCommand.get(nameOfCommand);
		LOG.trace("Obtained 'comand' = " + comand);

		// execute comand and get forward address
		String path = comand.execute(request, response, actionType);

		if (path == null) {
			if (!nameOfCommand.equals("downloadFile")) {
				LOG.trace("Redirect to address = " + path);
				LOG.debug("Controller proccessing finished");
				response.sendRedirect(AllPathes.PATH_WELCOME_PAGE);
			}
		} else {
			if (actionType == TypeAction.GET) {
				LOG.trace("Forward to address = " + path);
				LOG.debug("Controller proccessing finished");
				RequestDispatcher disp = request.getRequestDispatcher(path);
				disp.forward(request, response);
			} else if (actionType == TypeAction.POST) {
				LOG.trace("Redirect to address = " + path);
				LOG.debug("Controller proccessing finished");
				response.sendRedirect(path);

			}
		}
	}
}

package ua.nure.chechel.SummaryTask4.web.comands;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.web.TypeAction;

/**
 * Invoked when command was not found for client request.
 *
 * @author Sergey Chechel
 *
 */
public class CommandNo extends Command {
	private static final long serialVersionUID = -2785976616686657267L;

	private static final Logger LOG = Logger.getLogger(CommandNo.class);

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Command execution starts");

		String errorMessage = "No such command";
		request.setAttribute("errorMessage", errorMessage);
		LOG.error("Set the request attribute: 'errorMessage' = " + errorMessage);

		LOG.debug("Command execution finished");
		return AllPathes.PATH_ERROR_PAGE;
	}

}
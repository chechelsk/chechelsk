package ua.nure.chechel.SummaryTask4.web.comands;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.web.TypeAction;

/**
 * Invokes when user want to logout.
 * 
 * @author Sergey Chechel
 *
 */
public class CommandLogout extends Command {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(CommandLogout.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");
		String result = null;

		if (TypeAction.GET == actionType) {
			result = doGet(request, response);
		}

		LOG.debug("Finished executing Command");
		return result;
	}

	/**
	 * Forward to login page after logout.
	 * 
	 * @return path to login page
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
		}

		return AllPathes.PATH_WELCOME_PAGE;
	}
}

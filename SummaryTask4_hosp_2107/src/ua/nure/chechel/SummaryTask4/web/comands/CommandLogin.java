package ua.nure.chechel.SummaryTask4.web.comands;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.core.user.Role;
import ua.nure.chechel.SummaryTask4.core.user.User;
import ua.nure.chechel.SummaryTask4.core.user.UserManager;
import ua.nure.chechel.SummaryTask4.core.user.UserManagerImpl;
import ua.nure.chechel.SummaryTask4.web.TypeAction;

/**
 * Invoked when user logins in the system.
 *
 * @author Sergey Chechel.
 *
 */
public class CommandLogin extends Command {
	private static final long serialVersionUID = -3071536593627692473L;

	private static final Logger LOG = Logger.getLogger(CommandLogin.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");

		String result = null;

		if (actionType == TypeAction.POST) {
			result = doPost(request, response);
		} else {
			result = null;
		}

		LOG.debug("End executing command");
		return result;
	}

	/**
	 * Logins user in system. As first page displays depends on the user role.
	 *
	 * @return path to the page.
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response) {
		String forward = null;

		HttpSession session = request.getSession();

		String login = request.getParameter("login");
		String password = request.getParameter("password");

		UserManager manager = new UserManagerImpl();
		User user = manager.getUserByLogin(login);
		LOG.trace("User found: " + user);

		if (user == null || !password.equals(user.getPassword())) {
			String lang = (String) request.getSession().getAttribute("lang");
			String errorMessage = "";
			if (lang == null || lang.equals("en")) {
				errorMessage = "Cannot find user with such login/password";
			} else if (lang.equals("uk")) {
				errorMessage = "Не знайдено користувача з таким логіном/паролем";
			}
			request.getSession().setAttribute("error", errorMessage);
			LOG.error("errorMessage: Cannot find user with such login/password");
		} else {
			Role userRole = Role.getRole(user);
			LOG.trace("userRole --> " + userRole);

			if (userRole == Role.ADMIN) {
				forward = AllPathes.PATH_RDRT_TO_VIEW_ALL_DOCTORS;
			}

			if (userRole == Role.DOCTOR) {
				forward = AllPathes.PATH_RDRT_TO_VIEW_PATIENTS_BY_DOCTOR_ID;
			}

			if (userRole == Role.NURSE) {
				forward = AllPathes.PATH_RDRT_TO_VIEW_ALL_PATIENTS;
			}

			session.setAttribute("user", user);
			LOG.trace("Set the session attribute: user --> " + user);

			session.setAttribute("userRole", userRole);
			LOG.trace("Set the session attribute: userRole --> " + userRole);

			LOG.info("User " + user + " logged as " + userRole.toString().toLowerCase());
		}
		return forward;
	}

}
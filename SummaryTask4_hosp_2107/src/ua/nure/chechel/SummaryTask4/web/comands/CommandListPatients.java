package ua.nure.chechel.SummaryTask4.web.comands;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.core.patient.Patient;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManager;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManagerImpl;
import ua.nure.chechel.SummaryTask4.core.user.User;
import ua.nure.chechel.SummaryTask4.core.user.UserManager;
import ua.nure.chechel.SummaryTask4.core.user.UserManagerImpl;
import ua.nure.chechel.SummaryTask4.web.TypeAction;

/**
 * List of all patients in the hospital command.
 * 
 * @author Sergey Chechel
 *
 */
public class CommandListPatients extends Command {
	private static final long serialVersionUID = -2063322913213017032L;

	private static final Logger LOG = Logger.getLogger(CommandListPatients.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");
		String result = null;

		if (TypeAction.GET == actionType) {
			result = doGet(request, response);
		}
		LOG.debug("Command finished");
		return result;
	}

	/**
	 * Forward to list of all patients page.
	 * 
	 * @return path to page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		PatientManager manager = new PatientManagerImpl();
		List<Patient> patients = manager.getAllPatientsWithCountOfTreatments();
		LOG.trace("Count of patients: " + patients.size());

		request.setAttribute("patients", patients);
		
		UserManager userManager = new UserManagerImpl();
		Collection<User> doctors = userManager.getDoctors();
		request.setAttribute("doctors", doctors);

		return AllPathes.PATH_FORWARD_VIEW_ALL_PATIENTS;
	}

}

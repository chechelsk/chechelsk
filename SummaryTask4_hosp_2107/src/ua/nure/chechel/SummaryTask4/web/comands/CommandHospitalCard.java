package ua.nure.chechel.SummaryTask4.web.comands;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.core.patient.HospCard;
import ua.nure.chechel.SummaryTask4.core.patient.Patient;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManager;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManagerImpl;
import ua.nure.chechel.SummaryTask4.core.patient.Treatment;
import ua.nure.chechel.SummaryTask4.core.patient.TypeOfTreatment;
import ua.nure.chechel.SummaryTask4.web.TypeAction;
import ua.nure.chechel.SummaryTask4.web.utils.validation.HospitalCardInputValidator;

/**
 * Hospital card command.
 * 
 * @author Sergey Chechel
 *
 */
public class CommandHospitalCard extends Command {
	private static final long serialVersionUID = -7519771294666045392L;

	private static final Logger LOG = Logger.getLogger(CommandHospitalCard.class);

	private PatientManager manager = new PatientManagerImpl();

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");

		String result = null;

		if (TypeAction.GET == actionType) {
			result = doGet(request, response);
		} else if (TypeAction.POST == actionType) {
			result = doPost(request, response);
		}

		LOG.debug("Finished executing Command");

		return result;
	}

	/**
	 * Forward to hospital card page.
	 * 
	 * @return path to hospital card page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {

		// get hospital card id
		int hospitalCardId;
		if (request.getParameter("hospitalCardId") == null) {
			hospitalCardId = (int) request.getSession().getAttribute("hospitalCardId");
		} else {

			// first reference to current hospital card
			hospitalCardId = Integer.parseInt(request.getParameter("hospitalCardId"));

			// set id to session for other references to current card
			request.getSession().setAttribute("hospitalCardId", hospitalCardId);
		}
		LOG.trace("Hospital card id: " + hospitalCardId);

		HospCard hospitalCard = manager.getHospCardById(hospitalCardId);
		LOG.trace("Hospital card entity: " + hospitalCard);
		
		Patient patient = manager.getPatientByHospCardId(hospitalCardId);
		LOG.trace("Patient entity: " + patient);

		List<Treatment> treatments = manager.getAllTreatmentsByCardId(hospitalCardId);
		LOG.trace("Treatments: " + treatments);

		// get type of treatments for add treatment form
		List<TypeOfTreatment> typeOfTreatments = manager.getAllTypesOfTreatment();
		LOG.trace("Type Of Treatments: " + typeOfTreatments);

		request.setAttribute("hospitalCard", hospitalCard);
		request.setAttribute("patient", patient);
		request.setAttribute("treatments", treatments);
		request.getSession().setAttribute("typesOfTreatments", typeOfTreatments);

		// error message if if fields not properly filled
		if (request.getParameter("error") != null) {
			String lang = (String) request.getSession().getAttribute("lang");
			String errorMessage = "";
			if (lang == null || lang.equals("en")) {
				errorMessage = "Inncorect input, try again";
			} else if (lang.equals("uk")) {
				errorMessage = "Не вірний ввод, спробуйте ще";
			}
			request.setAttribute("errorMessage", errorMessage);
		}

		return AllPathes.PATH_FORWARD_HOSPITAL_CARD;
	}

	/**
	 * Redirect to view current hospital card.
	 * 
	 * @return path to view hospital card.
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response) {
		String diagnosis = request.getParameter("diagnosis");

		boolean valid = HospitalCardInputValidator.validateDiagnosis(diagnosis);
		if (!valid) {
			LOG.trace("Diagnosis not valid");
			return AllPathes.PATH_RDRT_TO_VIEW_HOSPITAL_CARD + "&error=notValidDiagnosis";
		}

		LOG.trace("Diagnosis: " + diagnosis);

		int hospitalCardId = (int) request.getSession().getAttribute("hospitalCardId");
		LOG.trace("Hospital card id: " + hospitalCardId);

		manager.updateDiagnosisInHospCard(hospitalCardId, diagnosis);
		LOG.trace("Diagnosis was update");

		return AllPathes.PATH_RDRT_TO_VIEW_HOSPITAL_CARD;
	}

}

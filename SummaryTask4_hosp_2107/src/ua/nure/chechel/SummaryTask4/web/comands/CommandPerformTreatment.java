package ua.nure.chechel.SummaryTask4.web.comands;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManager;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManagerImpl;
import ua.nure.chechel.SummaryTask4.web.TypeAction;

/**
 * To perform treatment for current patient command. Invokes when doctor or
 * nurse finished treatment.
 * 
 * @author Sergey Chechel
 *
 */
public class CommandPerformTreatment extends Command {
	private static final long serialVersionUID = 6738405211563400773L;

	private static final Logger LOG = Logger.getLogger(CommandPerformTreatment.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");

		String result = null;

		if (TypeAction.POST == actionType) {
			result = doPost(request, response);
		}

		LOG.debug("Finished executing Command");
		return result;
	}

	/**
	 * Redirect to view hospital card current patient.
	 * 
	 * @return path to view hospital card.
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response) {
		int treatmentId = Integer.parseInt(request.getParameter("id"));
		LOG.trace("Treatment id: " + treatmentId);

		PatientManager manager = new PatientManagerImpl();
		manager.finishTreatment(treatmentId);

		return AllPathes.PATH_RDRT_TO_VIEW_HOSPITAL_CARD;
	}

}

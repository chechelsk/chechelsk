package ua.nure.chechel.SummaryTask4.web.comands;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.core.patient.HospCard;
import ua.nure.chechel.SummaryTask4.core.patient.Patient;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManager;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManagerImpl;
import ua.nure.chechel.SummaryTask4.core.patient.Treatment;
import ua.nure.chechel.SummaryTask4.core.patient.TypeOfTreatment;
import ua.nure.chechel.SummaryTask4.core.user.User;
import ua.nure.chechel.SummaryTask4.core.user.UserManager;
import ua.nure.chechel.SummaryTask4.core.user.UserManagerImpl;
import ua.nure.chechel.SummaryTask4.web.TypeAction;

/**
 * Complete the course of treatment command for specified patient.
 * 
 * @author Sergey Chechel
 *
 */
public class CommandCompleteTheCourseOfTreatment extends Command {

	private static final long serialVersionUID = -5220729644390411869L;

	private static final Logger LOG = Logger.getLogger(CommandCompleteTheCourseOfTreatment.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");

		String result = null;

		if (TypeAction.POST == actionType) {
			result = doPost(request, response);
		}

		LOG.debug("Finished executing Command");
		return result;
	}

	/**
	 * Redirect to view discharged patients after completing course of treatment
	 * for patient.
	 * 
	 * @return path to view all discharged patients.
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response) {
		PatientManager patientManager = new PatientManagerImpl();
		UserManager userManager = new UserManagerImpl();

		// get current hospital card id from session
		int hospitalCardId = (int) request.getSession().getAttribute("hospitalCardId");
		LOG.trace("Hospital card id: " + hospitalCardId);

		// get all data about current patient
		HospCard hospitalCard = patientManager.getHospCardById(hospitalCardId);
		List<Treatment> treatments = patientManager.getAllTreatmentsByCardId(hospitalCardId);
		Patient patient = patientManager.getPatientByHospCardId(hospitalCardId);
		LOG.trace("Patient data: id: " + patient.getId() + ", firstName: " + patient.getFirstName() + ", lastName: "
				+ patient.getLastName() + ",date: " + patient.getBirthday() + ", cardId: " + patient.getCardId()
				+ ", doctorID: " + patient.getDoctorId());
		User doctor = userManager.getUserById(patient.getDoctorId());

		// write data to file  C:\eclipse\SummaryTask4_hosp_1407\WebContent\WEB-INF\DischargedPatients
		try {
			PrintWriter writer = new PrintWriter("C:\\eclipse\\SummaryTask4_hosp_2107\\WebContent\\WEB-INF\\DischargedPatients\\" 
					+ patient.getFirstName() + patient.getLastName() + ".txt", "UTF-8");

			writer.write("FirstName: " + patient.getFirstName() + "\r\n");
			writer.write("LastName: " + patient.getLastName() + "\r\n");
			writer.write("Diagmosis: " + hospitalCard.getDiagnosis() + "\r\n");
			writer.write("Doctor: " + doctor.getFirstName() + " " + doctor.getLastName() + "\r\n");
			writer.write("Treatments:\r\n");

			// write all history of treatment
			for (Treatment t : treatments) {
				writer.write(" TypeOfTreatment: " + TypeOfTreatment.getTypeOfTreatment(t) + " | Name: "
						+ t.getNameOfMedication() + " | isDone: " + t.isDone() + "\r\n");
			}

			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			LOG.error("Can not write data to file");
		}

		// complete course of treatment
		patientManager.completeCourseOfTreatment(patient);

		return AllPathes.PATH_RDRT_TO_VIEW_DISCHARGED_PATIENTS;
	}

}

package ua.nure.chechel.SummaryTask4.web.comands;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.core.patient.Patient;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManager;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManagerImpl;
import ua.nure.chechel.SummaryTask4.web.TypeAction;
import ua.nure.chechel.SummaryTask4.web.utils.validation.PatientInputValidator;

/**
 * Add patient command.
 * 
 * @author Sergey Chechel
 *
 */
public class CommandAddPatient extends Command {

	private static final long serialVersionUID = 2141162477716480717L;

	private static final Logger LOG = Logger.getLogger(CommandAddPatient.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");

		String result = null;

		if (TypeAction.GET == actionType) {
			result = doGet(request, response);
		} else if (TypeAction.POST == actionType) {
			result = doPost(request, response);
		}

		LOG.debug("Finished executing Command");

		return result;
	}

	/**
	 * Forwards to add patient form.
	 *
	 * @return path to the add patient page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		LOG.trace("Request for only showing addPatient.jsp");

		// error message if fields not properly filled
		if (request.getParameter("error") != null) {
			String lang = (String) request.getSession().getAttribute("lang");
			String errorMessage = "";
			if (lang == null || lang.equals("en")) {
				errorMessage = "Incorrect input or date, try again";
			} else if (lang.equals("uk")) {
				errorMessage = "Не вірний ввод, або дата, спробуйте ще";
			}
			request.setAttribute("errorMessage", errorMessage);
		}

		return AllPathes.PATH_FORWARD_PATIENT_ADD;
	}

	/**
	 * Redirects user after submitting add patient form.
	 *
	 * @return path to the view of added patients if fields properly filled,
	 *         otherwise redisplays add patient page.
	 */
	private String doPost(HttpServletRequest request, HttpServletResponse response) {

		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		Date selectedDate = null;

		// get selected birthday
		try {
			java.util.Date date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("date"));
			selectedDate = new Date(date.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}

		boolean valid = PatientInputValidator.validatePatientParametrs(firstName, lastName, selectedDate);

		if (valid) {

			LOG.trace("Fields were got: " + firstName + "," + lastName + ", " + selectedDate);

			Patient patient = new Patient(firstName, lastName, selectedDate);

			PatientManager manager = new PatientManagerImpl();
			int hospitalCardId = manager.addHospCard();
			patient.setCardId(hospitalCardId);
			manager.addPatient(patient);
			LOG.trace("The Patient was added to database");
		} else {
			LOG.trace("Fields not properly filled");
			return AllPathes.PATH_RDRT_TO_VIEW_ADD_PATIENT_FORM + "&error=notValid";
		}

		return AllPathes.PATH_RDRT_TO_VIEW_ALL_PATIENTS;
	}

}

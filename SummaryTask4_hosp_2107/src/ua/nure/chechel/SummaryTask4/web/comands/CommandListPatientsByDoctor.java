package ua.nure.chechel.SummaryTask4.web.comands;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.core.patient.Patient;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManager;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManagerImpl;
import ua.nure.chechel.SummaryTask4.core.user.User;
import ua.nure.chechel.SummaryTask4.web.TypeAction;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * List patients by doctor id command.
 * 
 * @author Sergey Chechel
 *
 */
public class CommandListPatientsByDoctor extends Command {

	private static final long serialVersionUID = -405445733126752744L;

	private static final Logger LOG = Logger.getLogger(CommandListPatientsByDoctor.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");
		String result = null;

		if (TypeAction.GET == actionType) {
			result = doGet(request, response);
		}

		LOG.debug("Command finished");
		return result;
	}

	/**
	 * Forward to list of patients by doctor id page.
	 * 
	 * @return path to list of patients page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {

		// get doctor id
		User doctor = (User) request.getSession().getAttribute("user");
		int doctorId = doctor.getId();
		LOG.trace("Doctor id: " + doctorId);

		// find patients by doctor id
		PatientManager manager = new PatientManagerImpl();
		List<Patient> patients = manager.getAllPatientsByDoctorId(doctorId);
		LOG.trace("Patients were found by doctor id: " + patients);

		request.setAttribute("patients", patients);

		return AllPathes.PATH_FORWARD_VIEW_ALL_PATIENTS;
	}

}

package ua.nure.chechel.SummaryTask4.web.comands;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.core.patient.Patient;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManager;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManagerImpl;
import ua.nure.chechel.SummaryTask4.core.user.User;
import ua.nure.chechel.SummaryTask4.web.TypeAction;

/**
 * List discharged patients command.
 * 
 * @author Sergey Chechel
 *
 */
public class CommandListDischargedPatients extends Command {
	private static final long serialVersionUID = -6955018370847324597L;

	private static final Logger LOG = Logger.getLogger(CommandListDischargedPatients.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");

		String result = null;

		if (TypeAction.GET == actionType) {
			result = doGet(request, response);
		}

		LOG.debug("Finished executing Command");

		return result;
	}

	/**
	 * Forward to discharged patients page.
	 * 
	 * @return path to discharged patients page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {

		// get doctor id
		User doctor = (User) request.getSession().getAttribute("user");
		int doctorId = doctor.getId();
		LOG.trace("Doctor id: " + doctorId);

		// find discharged patients by current doctor id
		PatientManager manager = new PatientManagerImpl();
		Collection<Patient> dischargedPatients = manager.getDischargedPatientsByDoctorId(doctorId);
		LOG.trace("Discarged patients: " + dischargedPatients);

		request.setAttribute("patients", dischargedPatients);

		return AllPathes.PATH_FORWARD_VIEW_DISCHARGED_PATIENTS;
	}

}

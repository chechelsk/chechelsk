package ua.nure.chechel.SummaryTask4.web.comands;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.core.user.User;
import ua.nure.chechel.SummaryTask4.core.user.UserManager;
import ua.nure.chechel.SummaryTask4.core.user.UserManagerImpl;
import ua.nure.chechel.SummaryTask4.web.TypeAction;

/**
 * List doctors by specialization command.
 * 
 * @author Sergey Chechel
 *
 */
public class CommandListDoctorsBySpecialization extends Command {
	private static final long serialVersionUID = -7126747068896099191L;

	private static final Logger LOG = Logger.getLogger(CommandListDoctorsBySpecialization.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");

		String result = null;

		if (TypeAction.GET == actionType) {
			result = doGet(request, response);
		}

		LOG.debug("Finished executing Command");
		return result;
	}

	/**
	 * Forward user to page of all doctors. View type depends on the user id.
	 *
	 * @return to view of all doctors by specialization.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		int specializationId = Integer.parseInt(request.getParameter("specializationId"));

		UserManager manager = new UserManagerImpl();

		Collection<User> doctors = manager.getDoctorsBySpecialization(specializationId);
		LOG.trace("Doctors found by specialization: " + doctors);

		request.setAttribute("doctors", doctors);

		return AllPathes.PATH_FORWARD_VIEW_ALL_DOCTORS;
	}

}

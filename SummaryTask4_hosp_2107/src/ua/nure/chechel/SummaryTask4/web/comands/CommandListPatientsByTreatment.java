package ua.nure.chechel.SummaryTask4.web.comands;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.core.patient.Patient;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManager;
import ua.nure.chechel.SummaryTask4.core.patient.PatientManagerImpl;
import ua.nure.chechel.SummaryTask4.web.TypeAction;

public class CommandListPatientsByTreatment extends Command {

	private static final long serialVersionUID = 1635079837283036506L;

	private static final Logger LOG = Logger.getLogger(CommandListPatientsByTreatment.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");
		String result = null;

		if (TypeAction.GET == actionType) {
			result = doGet(request, response);
		} else {
			result = doPost(request, response);
		}

		LOG.debug("Command finished");
		return result;
	}

	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		PatientManager manager = new PatientManagerImpl();

		// get patients by treatment type operation
		List<Patient> patients = manager.getAllPatientsByTreatmentOperation();

		request.setAttribute("patients", patients);

		return AllPathes.PATH_FORWARD_GET_PATIENTS;
	}

	private String doPost(HttpServletRequest request, HttpServletResponse response) {

		// get doctor and patient id
		int patientId = Integer.parseInt(request.getParameter("patientId"));
		int doctorId = Integer.parseInt(request.getParameter("doctorId"));
		LOG.trace("Patient id: " + patientId + " Doctor id: " + doctorId);

		LOG.trace("Trying appoint doctor to the patient");
		PatientManager manager = new PatientManagerImpl();
		manager.setDoctorToThePatient(patientId, doctorId);
		LOG.trace("The doctor was appointed to the patient");

		return AllPathes.PATH_RDRT_TO_VIEW_ALL_PATIENTS;
	}

}

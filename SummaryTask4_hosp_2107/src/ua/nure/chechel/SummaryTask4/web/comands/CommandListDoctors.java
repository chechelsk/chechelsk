package ua.nure.chechel.SummaryTask4.web.comands;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.core.user.Specialization;
import ua.nure.chechel.SummaryTask4.core.user.User;
import ua.nure.chechel.SummaryTask4.core.user.UserManager;
import ua.nure.chechel.SummaryTask4.core.user.UserManagerImpl;
import ua.nure.chechel.SummaryTask4.web.TypeAction;

/**
 * List of doctors command.
 * 
 * @author Sergey Chechel
 *
 */
public class CommandListDoctors extends Command {
	private static final long serialVersionUID = 7723206619340362128L;

	private static final Logger LOG = Logger.getLogger(CommandListDoctors.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, TypeAction actionType)
			throws IOException, ServletException {
		LOG.debug("Start executing Command");

		String result = null;

		if (TypeAction.GET == actionType) {
			result = doGet(request, response);
		}

		LOG.debug("Finished executing Command");
		return result;
	}

	/**
	 * Forward user to page of all doctors.
	 *
	 * @return path to view of all doctors page.
	 */
	private String doGet(HttpServletRequest request, HttpServletResponse response) {
		
		UserManager manager = new UserManagerImpl();

		Collection<User> doctors = manager.getDoctors();
		LOG.trace("Doctors found: " + doctors);

		Collection<Specialization> specializations = manager.getSpecializations();
		LOG.trace("Specializations found: " + specializations);

		request.setAttribute("doctors", doctors);
		request.getSession().setAttribute("specializations", specializations);

		return AllPathes.PATH_FORWARD_VIEW_ALL_DOCTORS;
	}

}

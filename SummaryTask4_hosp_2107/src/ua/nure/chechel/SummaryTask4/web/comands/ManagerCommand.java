package ua.nure.chechel.SummaryTask4.web.comands;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * Class that manages all comands.
 *
 * @author Sergey Chechel
 *
 */
public class ManagerCommand {

	private static final Logger LOG = Logger.getLogger(ManagerCommand.class);

	private static Map<String, Command> comands = new HashMap<String, Command>();

	// initialization all comands
	static {
		// common comands
		comands.put("login", new CommandLogin());
		comands.put("logout", new CommandLogout());
		comands.put("language", new CommandLanguage());
		comands.put("noCommand", new CommandNo());

		// doctor comands
		comands.put("listPatientsByDoctorId", new CommandListPatientsByDoctor());
		comands.put("hospitalCard", new CommandHospitalCard());
		comands.put("addTreatment", new CommandAddTreatment());
		comands.put("compleateTreatment", new CommandPerformTreatment());
		comands.put("listDischargedPatients", new CommandListDischargedPatients());
		comands.put("compleateCourseOfTreatment", new CommandCompleteTheCourseOfTreatment());
		comands.put("downloadFile", new CommandDownloadFile());
		
		//task
		comands.put("listPatientByTreatment", new CommandListPatientsByTreatment());

		//admin comands
		comands.put("listDoctors", new CommandListDoctors());
		comands.put("listDoctorsBySpecialization", new CommandListDoctorsBySpecialization());
		comands.put("addUser", new CommandAddUser());
		comands.put("addPatient", new CommandAddPatient());
		comands.put("appointDoctor", new CommandAppointDoctor());
		comands.put("listPatients", new CommandListPatients());

		LOG.debug("Command container was successfully initialized");
		LOG.trace("Total number of comands equals to " + comands.size());
	}

	/**
	 * Returns command object which execution will give path to the resource.
	 *
	 * @param commandName
	 *            Name of the command.
	 * @return Command object if container contains such command, otherwise
	 *         specific <code>noCommand</code> object will be returned.
	 */
	public static Command get(String commandName) {
		if (commandName == null || !comands.containsKey(commandName)) {
			LOG.trace("Command not found with name = " + commandName);
			return comands.get("noCommand");
		}

		return comands.get(commandName);
	}

}
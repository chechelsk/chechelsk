package ua.nure.chechel.SummaryTask4.web;

/**
 * Constants, which main purpose is to tell whether user wants to read or write
 * some data.
 *
 * @author Sergey Chechel
 *
 */
public enum TypeAction {
	GET, POST;
}

package ua.nure.chechel.SummaryTask4.web.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.chechel.SummaryTask4.AllPathes;
import ua.nure.chechel.SummaryTask4.core.user.Role;

/**
 * Invokes when user whant to execute some comand.
 * 
 * @author Sergey Chechel
 *
 */
public class FilterAccess implements Filter {
	private static final Logger LOG = Logger.getLogger(FilterAccess.class);

	// comands access
	private static Map<Role, List<String>> accessMap = new HashMap<Role, List<String>>();
	private static List<String> commons = new ArrayList<String>();
	private static List<String> outOfControl = new ArrayList<String>();

	public void destroy() {
		LOG.debug("starts Filter destruction ");
		// do nothing
		LOG.debug("finished Filter destruction ");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		LOG.debug("starts Filter");

		if (accessAllowed(request)) {
			LOG.debug("Filter finished: access allowed");
			chain.doFilter(request, response);
		} else {
			String errorMessasge = "For access to requested resource you don`t have permission";

			request.setAttribute("errorMessage", errorMessasge);
			LOG.trace("Set request attribute: errorMessage --> " + errorMessasge);

			request.getRequestDispatcher(AllPathes.PATH_ERROR_PAGE).forward(request, response);
		}
	}

	private boolean accessAllowed(ServletRequest request) {
		HttpServletRequest httpRequest = (HttpServletRequest) request;

		String nameOfCommand = request.getParameter("comand");
		LOG.trace("Command name --> " + nameOfCommand);
		if (nameOfCommand == null || nameOfCommand.isEmpty()) {
			return false;
		}
		if (outOfControl.contains(nameOfCommand)) {
			return true;
		}
		HttpSession session = httpRequest.getSession(false);
		if (session == null) {
			return false;
		}
		Role userRole = (Role) session.getAttribute("userRole");
		if (userRole == null) {
			return commons.contains(nameOfCommand);
		}
		return accessMap.get(userRole).contains(nameOfCommand) || commons.contains(nameOfCommand);
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		LOG.debug("starts Filter initialization");

		// initialization of commands by roles
		accessMap.put(Role.ADMIN, asList(fConfig.getInitParameter("admin")));
		accessMap.put(Role.DOCTOR, asList(fConfig.getInitParameter("doctor")));
		accessMap.put(Role.NURSE, asList(fConfig.getInitParameter("nurse")));
		LOG.trace("Map of Access --> " + accessMap);

		// initialization of common commands 
		commons = asList(fConfig.getInitParameter("common"));
		LOG.trace("Common comands --> " + commons);

		// initialization out of control commands
		outOfControl = asList(fConfig.getInitParameter("out-of-control"));
		LOG.trace("Out of control comands --> " + outOfControl);

		LOG.debug("Filter initialization finished");
	}

	/**
	 * Extracts parameter values from string.
	 * 
	 * @param str
	 *            string parameter values .
	 * @return list of parameter values.
	 */
	private List<String> asList(String str) {
		List<String> list = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(str);
		while (st.hasMoreTokens()) {
			list.add(st.nextToken());
		}
		return list;
	}
}
